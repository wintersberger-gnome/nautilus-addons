#!/bin/bash

IFS=$'\n'
for in_file in "${NAUTILUS_SCRIPT_SELECTED_FILE_PATHS}"; do
  in_file="`echo ${in_file} | sed s/\n$//g`"
  out_file="${in_file}.rtf"
  highlight -O rtf \
            -i "$in_file" \
            -o "$out_file" \
            -l \
            --font "LM Mono 10" \
            --font-size 10 \
            --style=zellner
done
