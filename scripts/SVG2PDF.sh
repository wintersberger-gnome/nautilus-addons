#!/bin/bash

IFS=$'\n'
for in_file in $NAUTILUS_SCRIPT_SELECTED_FILE_PATHS; do
    # TODO:
    # - check if the input file is really an SVG file
    # - check if the output file exists and issue a warning if this is the 
    #   case (we could use zenity to create the dialog)
    # - we have to check the file extension as the creation of the output file
    #   name depends on the fact that the input file has an extension .svg.
    #   If this is not the case we may have to omit this very file.

  in_file="`echo ${in_file} | sed s/\n$//g`"
  out_file=`echo $in_file | sed s/\.svg$/\.pdf/g`
  inkscape --without-gui \
           --file=$in_file \
           --export-pdf="$out_file"
done
