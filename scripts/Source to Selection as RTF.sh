#!/bin/bash

IFS=$'\n'
for in_file in $NAUTILUS_SCRIPT_SELECTED_FILE_PATHS; do
  in_file="`echo ${in_file} | sed s/\n$//g`"
  highlight -O rtf \
            -i $in_file \
            --stdout \
            -l \
            --font "LM Mono 10" \
            --font-size 10 \
            --style=zellner | xclip -t text/rtf -selection clipboard -i 
done
